import scrapy
import logging
import time
#import datetime
import urllib
import requests
#import geocoder
import re
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pyvirtualdisplay import Display
from selenium.webdriver.common.keys import Keys
import pandas as pd
import numpy as np
#from fake_useragent import UserAgent
#import random
#import traceback
import json
import sys
import linecache
from os import system
import os


class Michaellett(scrapy.Spider):           
    name = "michaellett"
    img_folder = ""
    path = "image_data"
    system('mkdir ' + path)
    requests_headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36'} # This is chrome, you can set whatever browser you like
    columns = ['title','artists', 'startdate', 'enddate', 'description', 'caption', 'image', 'veneue']
    
    if (os.path.isfile('michaellett.csv')):
        df = pd.read_csv("michaellett.csv", sep=",", index_col=0)
    else:
        df = pd.DataFrame(columns=columns) 

    logging.getLogger('scrapy').propagate = False
    logging.getLogger('selenium').propagate = False
    last_crawled_record_loc = 0
    #logging.getLogger('urllib3').propagate = False
    #logging.getLogger('requests').propagate = False
    logging.getLogger('scrapy').propagate = False
    
    def phantom_settings_function(self):
        # PhantomJS settings
        phantom_settings = dict(DesiredCapabilities.PHANTOMJS)
        phantom_settings['phantomjs.page.settings.userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['User-Agent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['phantomjs.page.settings.javascriptEnabled'] = True,   
        phantom_settings['phantomjs.page.settings.loadImages'] = True,
        phantom_settings['browserName'] = 'Google Chrome'
        phantom_settings['platformName'] = 'Linux'
        return phantom_settings
        
    def init_phantomjs_driver(self, *args, **kwargs):
    
        headers = { 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language':'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0',
            'Connection': 'keep-alive'
        }
    
        for key, value in enumerate(headers):
            webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.customHeaders.{}'.format(key)] = value
    
        webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
    
        driver =  webdriver.PhantomJS(*args, **kwargs)
        driver.set_window_size(1400,1000)
        print(webdriver.DesiredCapabilities.PHANTOMJS)
        return driver
        
    def __init__(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        self.options = Options()
        self.options.add_argument("headless")
        self.options.add_argument('--disable-gpu')
        self.options.headless = True
        self.options.set_headless(headless=True)
        self.options.add_argument("headless") # Runs Chrome in headless mode.
        self.options.add_argument('--no-sandbox') # Bypass OS security model
        self.options.add_argument('start-maximized') # 
        self.options.add_argument('disable-infobars')
        self.options.add_argument("--disable-extensions")
        self.prefs = {"profile.managed_default_content_settings.images": 2}
        self.options.add_experimental_option("prefs", self.prefs)
        self.headers = {
            'Accept':'*/*',
            'Accept-Encoding':'gzip, deflate, sdch',
            'Accept-Language':'en-US,en;q=0.8',
            'Cache-Control':'max-age=0',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
        }
        for key, value in enumerate(self.headers):
            capability_key = 'phantomjs.page.customHeaders.{}'.format(key)
            webdriver.DesiredCapabilities.PHANTOMJS[capability_key] = value
        self.driver = webdriver.Chrome(chrome_options=self.options)#(desired_capabilities=self.phantom_settings_function(),service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])
        #self.driver = self.init_phantomjs_driver()
        #self.driver = webdriver.PhantomJS(desired_capabilities =self.phantom_settings_function(), service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])

    def stale_aware_for_action(self, action):
        while(True):
            try:
                action()
                break
            except StaleElementReferenceException:
                continue
            
    def start_requests(self):
        urls = [
            "http://michaellett.com/exhibition/"
            ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, priority = 10)
    
    def PrintException(self):
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))
    
    
    def if_exist_in_file(self, job_id):
        if job_id in self.file_data:
            return True
        return False
        
    def update_file(self, content=""):
        open("records.txt", 'w').close()
        f = open("records.txt", "a+")
        for data in self.current_data:
            f.seek(0,0)
            f.write('\n' + data + '\n' )
        
    def create_job_array_from_existing_jobs(self):
        f = open("records.txt", "r+")
        for line in f.read().split('\n'):
            self.file_data.insert(0,line)
    
    def saveToFile(self, fName, i):
        with open(fName, "w+") as f:
            f.write(str(i))

    def readFromFile(self, fName):
        try:
            f = open(fName, 'r')
            content = f.read()
            if len(content) > 0:
                return int(content)
            return 0
        except IOError:
            open(fName, 'w+').close()
            return 0
        
        
        with open(fName, "r") as f:
            content = f.read()
            if len(content) > 0:
                return int(content)
            return 0

    def deleteFile(self, fName):
        with open(fName, "w+") as f:
            pass
            
    def get_true_text(self, tag):
        children = tag.find_elements_by_xpath('*')
        original_text = tag.text
        for child in children:
            original_text = original_text.replace(child.text, '', 1)
        return original_text    
    
    def check_exists_by_xpath(self,xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def check_exists_by_xpath_parent(self, parent, xpath, ):
        try:
            parent.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
    
    def date_switcher(self, date_string): # 07 November — 01 December 2018
        switcher = {
                "January" : "01",
                "February" : "02",
                "March" : "03",
                "April" : "04",
                "May" : "05",
                "June" : "06",
                "July" : "07",
                "August" : "08",
                "September" : "09",
                "October" : "10",
                "November" : "11",
                "December" : "12"
            }
        date_arr = date_string.split(' ')
        print(date_arr)
        startdate = date_arr[0] + '/' + switcher[date_arr[1]] + '/' + date_arr[-1]
        enddate = date_arr[3] + '/' + switcher[date_arr[4]] + '/' + date_arr[-1]
        
        return startdate, enddate

    
    def parse(self, response):
        self.driver.get(response.url)
        list_of_link = self.driver.find_elements_by_xpath('//*[@id="contentHolder"]/nav/div/div/div/p/a')
        # print(len(list_of_link))       
        self.last_crawled_record_loc = self.readFromFile("record_info.txt")
        for i in range(self.last_crawled_record_loc ,len(list_of_link)):
            print((str(i) + "\t") * 5)
            if (i == len(list_of_link)-57):
                pass
            else:
                title = ""
                date = ""
                artist = ""
                description = ""
                caption = ""
                artist_list = []
                veneue = 77
                time.sleep(1)
                list_of_link = self.driver.find_elements_by_xpath('//*[@id="contentHolder"]/nav/div/div/div/p/a')
                self.driver.execute_script("arguments[0].click();", list_of_link[i]) 
                time.sleep(1)
                
                img_list = self.driver.find_elements_by_xpath('//div[@class="col col-sm-12 singleWork"]')
                information_button = self.driver.find_element_by_xpath('//div[@class="col col-sm-2 bioOpen"]')
                information_button.click()
                description = self.driver.find_element_by_xpath('//div[@class="col col-sm-6 subCol bioCol"]').get_attribute('textContent').replace('\xa0','')
                
                # print(len(img_list))
                if (self.check_exists_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/h1/em')): # means there is title 
                    artist_box = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/h1')
                    title = artist_box.find_element_by_xpath('./em').get_attribute('textContent')
                    date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p').get_attribute('textContent').replace('\n', '').replace('\t','')
                    date_parsed = date.split(' ')
                    if len(date_parsed)  == 8:
                        date_parsed.pop(3)
                        date = ' '.join(date_parsed)
                    startdate, enddate = self.date_switcher(date[1:])
                    artist = artist_box.get_attribute('textContent').replace(title, '').replace('\n', '').replace('\t','')
                    artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                    for artist_data in artist_field:
                        artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                    artist = ','.join(artist_list)
                    
                elif (self.check_exists_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[1]/span/em')):
                    title = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[1]/span/em').get_attribute('textContent').replace('\xa0', '')
                    date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[2]').get_attribute('textContent').replace('\n', '').replace('\t','')
    
                    date_parsed = date.split(' ')
                    if len(date_parsed)  == 8:
                        date_parsed.pop(3)
                        date = ' '.join(date_parsed)
                        
                    startdate, enddate = self.date_switcher(date[1:])
                    
                    artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                    for artist_data in artist_field:
                        artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                    artist = ','.join(artist_list)
                    print(description)
                
                else:
                    artist_box = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]')
                    date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p').get_attribute('textContent')
                    date_parsed = date.split(' ')
                    if len(date_parsed)  == 8:
                        date_parsed.pop(3)
                        date = ' '.join(date_parsed)
                    startdate, enddate = self.date_switcher(date[1:])
                    artist = artist_box.get_attribute('textContent').replace(date,'').replace('\n', '').replace('\t','')
                    artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                    
                    for artist_data in artist_field:
                        artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                        artist = ','.join(artist_list)
                        
                print("\n" + ( "#" * 30 ) + "\n")
                print("title: {}\nartist: {}\ndate: {}".format(title, artist, date))
                
                self.img_folder = re.sub(r'[^\w\s]','',str(artist + title).replace(' ', '').replace('\\','').replace('/','').replace("\xa0", "").replace('\n',''))[0:250]

                system('mkdir ' + self.path + '\\' + self.img_folder)
                if self.img_folder == "JimAllenHangingbyaThreadII":
                    pass
                for j in range(len(img_list)):     
                    img_url = img_list[j].find_element_by_xpath('./img').get_attribute('src')
                    url_seperated = img_url.rsplit('-',1)
                    image_ext = url_seperated[1].split('.')[-1]
                    full_image_url = url_seperated[0] + '.' + image_ext
    
                    response_full = requests.get(full_image_url, headers = self.requests_headers)
                    if (self.check_exists_by_xpath_parent(img_list[j], './div[@class="captionHolder col-sm-6 col-sm-push-6"]')):
                        caption = img_list[j].find_element_by_xpath('./div[@class="captionHolder col-sm-6 col-sm-push-6"]').get_attribute('textContent')
    
                    if response_full.status_code == 200:
                        with open(self.path + "/" + self.img_folder + "/" + str(j) + '.jpg', 'wb') as f:
                            f.write(response_full.content)
                    else:
                        response_orig = requests.get(img_url, headers = self.requests_headers)
                        if response_orig.status_code == 200:
                            with open(self.path + "/" + self.img_folder + "/" + str(j) + '.jpg', 'wb') as f:
                                f.write(response_orig.content)
    
                    data = {
                            "title" : title.replace('\n', '').replace('\t', '').replace('\xa0',''),
                            "artists" : artist.replace('\n', '').replace('\t', '').replace('\xa0',''),
                            "startdate" : startdate,
                            "enddate" : enddate,
                            "description" : description.replace('\n', '').replace('\t', '').replace('\xa0',''),
                            "caption" : caption.replace('\n', '').replace('\t', '').replace('\xa0',''),
                            "image" : (self.path + "/" + self.img_folder + "/" + str(j) + '.jpg'),
                            "veneue": veneue
                            }
                    print(data)
                    
                    self.df = self.df.append([data], ignore_index=True)
                
                if (os.path.isfile('michaellett.csv')):
                    self.df.to_csv("{}.csv".format(self.name), sep=',', header=None)
                else:
                    self.df.to_csv("{}.csv".format(self.name), sep=',', header=True)
                
                print(self.df.tail())
                # after all jobs is done go back.
                self.deleteFile("record_info.txt")
                self.saveToFile("record_info.txt", i+1)
                self.driver.execute_script("window.history.go(-1)")
        
        #self.df.to_csv("{}.csv".format(self.name), sep=',')

        self.driver.close()
        self.display.stop()
        self.driver.quit()


class Michaellett_Current(scrapy.Spider):           
    name = "michaellett_current"
    img_folder = ""
    path = "image_data"
    system('mkdir ' + path)
    requests_headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36'} # This is chrome, you can set whatever browser you like
    columns = ['title','artists', 'startdate', 'enddate', 'description', 'caption', 'image', 'veneue']
    
    if (os.path.isfile('michaellett.csv')):
        df = pd.read_csv("michaellett.csv", sep=",", index_col=0)
    else:
        df = pd.DataFrame(columns=columns) 

    logging.getLogger('scrapy').propagate = False
    logging.getLogger('selenium').propagate = False
    last_crawled_record_loc = 0
    #logging.getLogger('urllib3').propagate = False
    #logging.getLogger('requests').propagate = False
    logging.getLogger('scrapy').propagate = False
    
    def phantom_settings_function(self):
        # PhantomJS settings
        phantom_settings = dict(DesiredCapabilities.PHANTOMJS)
        phantom_settings['phantomjs.page.settings.userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['User-Agent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['phantomjs.page.settings.javascriptEnabled'] = True,   
        phantom_settings['phantomjs.page.settings.loadImages'] = True,
        phantom_settings['browserName'] = 'Google Chrome'
        phantom_settings['platformName'] = 'Linux'
        return phantom_settings
        
    def init_phantomjs_driver(self, *args, **kwargs):
    
        headers = { 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language':'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0',
            'Connection': 'keep-alive'
        }
    
        for key, value in enumerate(headers):
            webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.customHeaders.{}'.format(key)] = value
    
        webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
    
        driver =  webdriver.PhantomJS(*args, **kwargs)
        driver.set_window_size(1400,1000)
        print(webdriver.DesiredCapabilities.PHANTOMJS)
        return driver
        
    def __init__(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        self.options = Options()
        self.options.add_argument("headless")
        self.options.add_argument('--disable-gpu')
        self.options.headless = True
        self.options.set_headless(headless=True)
        self.options.add_argument("headless") # Runs Chrome in headless mode.
        self.options.add_argument('--no-sandbox') # Bypass OS security model
        self.options.add_argument('start-maximized') # 
        self.options.add_argument('disable-infobars')
        self.options.add_argument("--disable-extensions")
        self.prefs = {"profile.managed_default_content_settings.images": 2}
        self.options.add_experimental_option("prefs", self.prefs)
        self.headers = {
            'Accept':'*/*',
            'Accept-Encoding':'gzip, deflate, sdch',
            'Accept-Language':'en-US,en;q=0.8',
            'Cache-Control':'max-age=0',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
        }
        for key, value in enumerate(self.headers):
            capability_key = 'phantomjs.page.customHeaders.{}'.format(key)
            webdriver.DesiredCapabilities.PHANTOMJS[capability_key] = value
        self.driver = webdriver.Chrome(chrome_options=self.options)#(desired_capabilities=self.phantom_settings_function(),service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])
        #self.driver = self.init_phantomjs_driver()
        #self.driver = webdriver.PhantomJS(desired_capabilities =self.phantom_settings_function(), service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])

    def stale_aware_for_action(self, action):
        while(True):
            try:
                action()
                break
            except StaleElementReferenceException:
                continue
            
    def start_requests(self):
        urls = [
            "http://michaellett.com/exhibition/"
            ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, priority = 10)
    
    def PrintException(self):
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))
    
    
    def if_exist_in_file(self, job_id):
        if job_id in self.file_data:
            return True
        return False
        
    def update_file(self, content=""):
        open("records.txt", 'w').close()
        f = open("records.txt", "a+")
        for data in self.current_data:
            f.seek(0,0)
            f.write('\n' + data + '\n' )
        
    def create_job_array_from_existing_jobs(self):
        f = open("records.txt", "r+")
        for line in f.read().split('\n'):
            self.file_data.insert(0,line)
    
    def saveToFile(self, fName, i):
        with open(fName, "w+") as f:
            f.write(str(i))

    def readFromFile(self, fName):
        try:
            f = open(fName, 'r')
            content = f.read()
            if len(content) > 0:
                return int(content)
            return 0
        except IOError:
            open(fName, 'w+').close()
            return 0
        
        
        with open(fName, "r") as f:
            content = f.read()
            if len(content) > 0:
                return int(content)
            return 0

    def deleteFile(self, fName):
        with open(fName, "w+") as f:
            pass
            
    def get_true_text(self, tag):
        children = tag.find_elements_by_xpath('*')
        original_text = tag.text
        for child in children:
            original_text = original_text.replace(child.text, '', 1)
        return original_text    
    
    def check_exists_by_xpath(self,xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def check_exists_by_xpath_parent(self, parent, xpath, ):
        try:
            parent.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
    
    def date_switcher(self, date_string): # 07 November — 01 December 2018
        switcher = {
                "January" : "01",
                "February" : "02",
                "March" : "03",
                "April" : "04",
                "May" : "05",
                "June" : "06",
                "July" : "07",
                "August" : "08",
                "September" : "09",
                "October" : "10",
                "November" : "11",
                "December" : "12"
            }
        date_arr = date_string.split(' ')
        print(date_arr)
        startdate = date_arr[0] + '/' + switcher[date_arr[1]] + '/' + date_arr[-1]
        enddate = date_arr[3] + '/' + switcher[date_arr[4]] + '/' + date_arr[-1]
        
        return startdate, enddate

    
    def parse(self, response):
        self.driver.get(response.url)
        
        #self.driver.find_element_by_xpath('//*[@id="mainNav"]/div/div/div/div[1]/div/button').click()
        time.sleep(5)
        current_link = self.driver.find_elements_by_xpath('//div[@class="col col-sm-5 menuCol exhibCol"]/p[2]/a')
        list_of_link = current_link

        for i in range(len(list_of_link)):
            title = ""
            date = ""
            artist = ""
            description = ""
            caption = ""
            artist_list = []
            veneue = 77
            time.sleep(1)

            current_link = self.driver.find_elements_by_xpath('//div[@class="col col-sm-5 menuCol exhibCol"]/p[2]/a')
            list_of_link = current_link
            self.driver.execute_script("arguments[0].click();", list_of_link[i]) 
            time.sleep(1)
            
            img_list = self.driver.find_elements_by_xpath('//div[@class="col col-sm-12 singleWork"]')
            information_button = self.driver.find_element_by_xpath('//div[@class="col col-sm-2 bioOpen"]')
            information_button.click()
            description = self.driver.find_element_by_xpath('//div[@class="col col-sm-6 subCol bioCol"]').get_attribute('textContent').replace('\xa0','')
            
            if (self.check_exists_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/h1/em')): # means there is title 
                artist_box = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/h1')
                title = artist_box.find_element_by_xpath('./em').get_attribute('textContent')
                date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p').get_attribute('textContent').replace('\n', '').replace('\t','')
                date_parsed = date.split(' ')
                if len(date_parsed)  == 8:
                    date_parsed.pop(3)
                    date = ' '.join(date_parsed)
                startdate, enddate = self.date_switcher(date[1:])
                artist = artist_box.get_attribute('textContent').replace(title, '').replace('\n', '').replace('\t','')
                artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                for artist_data in artist_field:
                    artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                artist = ','.join(artist_list)
                
            elif (self.check_exists_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[1]/span/em')):
                title = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[1]/span/em').get_attribute('textContent').replace('\xa0', '')
                date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p[2]').get_attribute('textContent').replace('\n', '').replace('\t','')

                date_parsed = date.split(' ')
                if len(date_parsed)  == 8:
                    date_parsed.pop(3)
                    date = ' '.join(date_parsed)
                    
                startdate, enddate = self.date_switcher(date[1:])
                
                artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                for artist_data in artist_field:
                    artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                artist = ','.join(artist_list)
                print(description)
            
            else:
                artist_box = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]')
                date = self.driver.find_element_by_xpath('//*[@id="contentHolder"]/article/section[1]/div/div/div[1]/p').get_attribute('textContent')
                date_parsed = date.split(' ')
                if len(date_parsed)  == 8:
                    date_parsed.pop(3)
                    date = ' '.join(date_parsed)
                startdate, enddate = self.date_switcher(date[1:])
                artist = artist_box.get_attribute('textContent').replace(date,'').replace('\n', '').replace('\t','')
                artist_field = self.driver.find_elements_by_xpath('//nav[@class="artistExhibNav"]/ul/li')
                
                for artist_data in artist_field:
                    artist_list.append(artist_data.get_attribute('textContent').replace('\n','').replace('\t',''))
                    artist = ','.join(artist_list)
                    
            print("\n" + ( "#" * 30 ) + "\n")
            print("title: {}\nartist: {}\ndate: {}".format(title, artist, date))
            
            self.img_folder = re.sub(r'[^\w\s]','',str(artist + title).replace(' ', '').replace('\\','').replace('/','').replace("\xa0", "").replace('\n',''))[0:250]

            system('mkdir ' + self.path + '/' + self.img_folder)

            for j in range(len(img_list)):     
                img_url = img_list[j].find_element_by_xpath('./img').get_attribute('src')
                url_seperated = img_url.rsplit('-',1)
                image_ext = url_seperated[1].split('.')[-1]
                full_image_url = url_seperated[0] + '.' + image_ext

                response_full = requests.get(full_image_url, headers = self.requests_headers)
                if (self.check_exists_by_xpath_parent(img_list[j], './div[@class="captionHolder col-sm-6 col-sm-push-6"]')):
                    caption = img_list[j].find_element_by_xpath('./div[@class="captionHolder col-sm-6 col-sm-push-6"]').get_attribute('textContent')

                if response_full.status_code == 200:
                    with open(self.path + "/" + self.img_folder + "/" + str(j) + '.jpg', 'wb') as f:
                        f.write(response_full.content)
                else:
                    response_orig = requests.get(img_url, headers = self.requests_headers)
                    if response_orig.status_code == 200:
                        with open(self.path + "/" + self.img_folder + "/" + str(j) + '.jpg', 'wb') as f:
                            f.write(response_orig.content)

                data = {
                        "title" : title.replace('\n', '').replace('\t', '').replace('\xa0',''),
                        "artists" : artist.replace('\n', '').replace('\t', '').replace('\xa0',''),
                        "startdate" : startdate,
                        "enddate" : enddate,
                        "description" : description.replace('\n', '').replace('\t', '').replace('\xa0',''),
                        "caption" : caption.replace('\n', '').replace('\t', '').replace('\xa0',''),
                        "image" : (self.path + "/" + self.img_folder + "/" + str(j) + '.jpg'),
                        "veneue": veneue
                        }
                print(data)
                
                self.df = self.df.append([data], ignore_index=True)
            
            if (os.path.isfile('michaellett.csv')):
                self.df.to_csv("{}.csv".format('michaellett'), sep=',', header=None)
            else:
                self.df.to_csv("{}.csv".format('michaellett'), sep=',', header=True)
            
            print(self.df.tail())

            self.driver.execute_script("window.history.go(-1)")
        
        #self.df.to_csv("{}.csv".format(self.name), sep=',')

        self.driver.close()
        self.display.stop()
        self.driver.quit()
